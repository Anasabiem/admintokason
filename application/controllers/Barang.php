<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Barang extends CI_Controller {
	function __construct()
    {
        parent::__construct();
        $this->load->model('ModelSRC');
    }
	public function index()
	{
		$data['detBarang']=$this->M_model->selectwhere('barang', array('stok < '=> 10))->result();
        $this->load->view('v_barang',$data);
    }
}
